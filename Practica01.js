function alumno(_numcontrol,_nombre){
    return {
    num_control:_numcontrol,
    nombre:_nombre,
    grupo:[]
    }
    }
    
//creamos alumnos
alumno1=alumno("091452","Morales Zarate Emanuel "); 
alumno2=alumno("096236","Daniela Flores Rodriguez ");  
alumno3=alumno("106253","Yahir Cruz SantibaÃ±ez ");  
alumno4=alumno("152734","fabian Budar Juan Carlos ");  
alumno5=alumno("101928","Maria Antonieta de las Nieves ");  
alumno6=alumno("091314","Cesar Hernanez Ruiz ");  
alumno7=alumno("097432","Juan Alejandro Martinez");  
alumno8=alumno("091722","Daniel  Morales Zarate ");  
alumno9=alumno("102732","Ricardo Perez Ramirez "); 
alumno10=alumno("192342","Sandra Martinez Pacheco ");   
alumno11=alumno("10160872","Rodrigo Jimenez Pacheco  ");  
alumno12=alumno("10160873","Irving Mendez Sanchez"); 
alumno13=alumno("11099291","Jesus Arellanees Ortiz ");      

//comprobamos que existan los objetos alumno
console.log(alumno7)
console.log(alumno6)
console.log(alumno5)

//funcion grupo que se encaerga de crear objetos de  grupo los cuales se asignaran alumnos
function grupo(_clave,_nombre){
    return{
    clave:_clave,
    nombre:_nombre,
    alumnos:[],
    materias:[],
    profesores:[],
    horarios:[]
    }
    }

//creamos un objeto de grupo

grupo1=grupo("pol","A");
//asignamos alumnos materias profesor y horarios al objeto de grupo
grupo1.alumnos[0]=alumno1
grupo1.alumnos[1]=alumno2
grupo1.alumnos[2]=alumno3
grupo1.alumnos[3]=alumno4
grupo1.alumnos[4]=alumno5
grupo1.alumnos[5]=alumno6
grupo1.alumnos[6]=alumno7
grupo1.alumnos[7]=alumno8
grupo1.alumnos[8]=alumno9
grupo1.alumnos[10]=alumno10

//podemos crear un segundo grupo
grupo2= grupo("Bls","B");

grupo2.alumnos[0]=alumno10;
grupo2.alumnos[1]= alumno11;
grupo2.alumnos[2]= alumno12;
grupo2.alumnos[3]= alumno2;
grupo2.alumnos[4]= alumno3;
grupo2.alumnos[5] = alumno4;
grupo2. alumnos[6]= alumno9;
grupo2. alumnos[7]=alumno1;

//agregamos un grupo a alumnos
alumno1.grupo=grupo1;
alumno2.grupo=grupo1;
alumno3.grupo=grupo1;
alumno4.grupo=grupo1;
alumno5.grupo=grupo1;
alumno6.grupo=grupo1;
alumno7.grupo=grupo1;
alumno8.grupo=grupo1;
alumno9.grupo=grupo1;
alumno10.grupo=grupo1;

//grupo2 a alumnos
alumno1.grupo=grupo2;
alumno2.grupo=grupo2;
alumno9.grupo=grupo2;
alumno10.grupo=grupo2;


//comprobamos que el alumno tendra grupo
console.log(alumno1["grupo"]);
console.log(alumno10["grupo"]);
console.log(grupo2[15]);


// Funcion para asignar materias con campos de nombre y clave 

function materia(_clave,_nombre){
        return{
        clave:_clave,
        nombre:_nombre
        }
    }
//creamos los objetos de materia    
    materia1=materia("1","Español");
    materia2=materia("2","Matematicas");
    materia3=materia("3","Historia");
    materia4=materia("4","Geografia");
    materia5=materia("5","Gramatica");
    
    materia6 =materia("6","Programacion");
    materia7=materia("7","Lectura y Redaccion");
    materia8= materia("8","Ingles I");
    materia9=materia("9","Poesia y cultura");


    //verificamos que existan los objetos de materia
    console.log(materia1.nombre)
    
//agregamos materias al grupo
grupo1.materias[0]=materia1;
grupo1.materias[1]=materia2;
grupo1.materias[2]=materia3;
grupo1.materias[3]=materia4;
grupo1.materias[4]=materia5;


grupo2.materias[0]=materia6;
grupo2.materias[1]=materia7;
grupo2.materias[2]=materia8;
//comprobamos que estÃƒÂ©n las materias
console.log(grupo1["materias"][1].nombre)
console.log(grupo2["materias"][2].nombre)


//Creamos los profsores que seran los que impartiran la materia 

function profesor(_clave,_nombre){
        return{
        clave:_clave,
        nombre:_nombre
        }
    }
    //creamos los objetos de profesor
     profesor1=profesor("1254","Alejandra Barros Quintero");
     profesor2=profesor("7352","Pedro Vasquez Rodriguez");
     profesor3=profesor("8156","Adan Mendoza Jimenez");
     profesor4=profesor("9823","Pedro Arreortua Diaz");
     profesor5=profesor("1452","Miguel Angel Garcia Garcia");
   
    //verificamos que existan los objetos de profesor
     console.log(profesor1)
     console.log(profesor2)
     
//agregamos profesores al grupo
grupo1.profesores[0]=profesor1;
grupo1.profesores[1]=profesor2;
grupo1.profesores[2]=profesor3;
grupo1.profesores[3]=profesor4;
grupo1.profesores[4]=profesor5;

grupo2.profesores[0]=profesor5;
grupo2.profesores[2]=profesor3;
grupo2.profesores[1]=profesor4;

//comprobamos que esten los profesores
console.log(grupo1["profesores"][0].nombre)
console.log(grupo2["profesores"][1].nombre)

//crea nuevos horarios    
function horario(_clave,_horaini,_horafin){
        return{
        clave:_clave,
        hora_ini:_horaini,
        hora_fin:_horafin,
        materia:[],
        profesor:[]
        }
    }
//creamos los objetos de horario
horario1=horario("abc","8:00","9:00");
horario2=horario("dsc","9:00","10:00");
horario3=horario("yrc","10:00","11:00");
horario4=horario("qpa","11:00","12:00");
horario5=horario("uys","12:00","13:00");

//verificamos
console.log(horario5.profesor)
//asignamos materia y profesor a horarios
horario1.materia=materia1;
horario1.profesor=profesor1;
horario2.materia=materia2;
horario2.profesor=profesor2;
horario3.materia=materia3;
horario3.profesor=profesor3;
horario4.materia=materia4;
horario4.profesor=profesor4;
horario5.materia=materia5;
horario5.profesor=profesor5;


//agreagamos horarios al grupo!!
grupo1.horarios[0]=horario1;
grupo1.horarios[1]=horario2;
grupo1.horarios[2]=horario3;
grupo1.horarios[3]=horario4;
grupo1.horarios[4]=horario5;
//comprobamos que esten los horarios en el grupo
console.log(grupo1["horarios"][3].hora_ini)



//*************funciones para el uso de los objetos y su informacion *************

function imprimir_lista_alumnos(grupo)
{
   for each (var alumno in grupo.alumnos)
    console.log(alumno.nombre)
     
}
imprimir_lista_alumnos(grupo1);

    


function imprimir_lista_horario(grupo){
    for each(var horario in grupo.horarios )
    console.log("Hora: "+horario.hora_ini+"-"+horario.hora_fin+" - Profesor: "+ horario.profesor.nombre+" - Materia: "+horario.materia.nombre )

}
 imprimir_lista_horario(grupo1);
 
 //funcion para ver si existe un alumno en el grupo, insertando el numero de control 
 function existe_alumno(grupo,a){
    for each(var alumno in grupo.alumnos)
    if (a==alumno.num_control)
    console.log(true)
    
}

existe_alumno(grupo1,"091452")

//funcion para ver si existe una materia en el grupo insertanto clave.
function existe_materia(grupo,a){
    for each(var materia in grupo.materias)
    if (a==materia.clave)
    console.log(true)
    
}
existe_materia(grupo1,"1")


//funcion para ver si existe una profesor en el grupo insertando clave.
function existe_profesor(grupo,a){
    for each(var profesor in grupo.profesores)
    if (a==profesor.clave)
    console.log(true)
    
}
existe_profesor(grupo1,"8156")


//insertar alumno

function insertar_alumno(num,nom){
    alumno(num,nom);
    num.grupo=grupo1;
    
}
insertar_alumno("7382","Alejandro Levis")


function eliminar_alumno(num)
{
    for each (var numcon in alumno.num_control)
    if (num==alumno.num_control)
    alumno.num_control=null;
    alumno.nombre=null;
    alumno.grupo=null;
    console.log("ha sido eliminado") ;
}

eliminar_alumno("091452")
console.log(alumno1)




